Feature: Manage events
  In order to [goal]
  [stakeholder]
  wants [behaviour]
  
  Scenario: Register new events
    Given I am on the new events page
    When I fill in "Title" with "title 1"
    When I select "food" from "Category"
    When I fill in "Description" with "description 1"
    And I fill in "By" with "by 1"
    When I fill in "Start" date field with "2019, Aug, 25"
    When I fill in "End" date field with "2019, Aug, 26"
    When I fill in "Phone" with "phone 1"
    And I press "Save"
    Then I should see "title 1"
    And I should see "food"
    And I should see "description 1"
    And I should see "by 1"
    And I should see "2019-08-25 "
    And I should see "2019-08-26 "
    And I should see "phone 1"

  Scenario: Delete event
    Given the following events:
      |title|category|description|by|start|end|phone|
      |title 1|category 1|description 1|by 1|2019-11-19 00:00:00 UTC|2019-11-19 00:00:00 UTC|phone 1|
      |title 2|category 2|description 2|by 2|2019-11-19 00:00:00 UTC|2019-11-19 00:00:00 UTC|phone 2|
      |title 3|category 3|description 3|by 3|2019-11-19 00:00:00 UTC|2019-11-19 00:00:00 UTC|phone 3|
      |title 4|category 4|description 4|by 4|2019-11-19 00:00:00 UTC|2019-11-19 00:00:00 UTC|phone 4|
    
    When I delete the 3rd event
    And I should see the following events:
      |Title|Category|Description|By|Start|End|Phone||||
      |title 1|category 1|description 1|by 1|2019-11-19 00:00:00 UTC|2019-11-19 00:00:00 UTC|phone 1|Show|Edit|Destroy|
      |title 2|category 2|description 2|by 2|2019-11-19 00:00:00 UTC|2019-11-19 00:00:00 UTC|phone 2|Show|Edit|Destroy|
      |title 4|category 4|description 4|by 4|2019-11-19 00:00:00 UTC|2019-11-19 00:00:00 UTC|phone 4|Show|Edit|Destroy|
