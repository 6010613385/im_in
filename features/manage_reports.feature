Feature: Manage reports
  In order to [goal]
  [stakeholder]
  wants [behaviour]
  
  Scenario: Register new reports
    Given I am on the new reports page
    When I fill in "Event" with "event 1"
    And I fill in "Report to" with "report_to 1"
    And I fill in "By" with "by 1"
    And I fill in "Desciption" with "desciption 1"
    And I press "Save"
    Then I should see "event 1"
    And I should see "report_to 1"
    And I should see "by 1"
    And I should see "desciption 1"
    
    Scenario: Delete report
    Given the following reports:
      |event|report_to|by|desciption|
      |event 1|report_to 1|by 1|desciption 1|
      |event 2|report_to 2|by 2|desciption 2|
      |event 3|report_to 3|by 3|desciption 3|
      |event 4|report_to 4|by 4|desciption 4|
      
    When I delete the 2rd report
    Then I should see the following reports:
      |Event|Report To|By|Desciption||||
      |event 1|report_to 1|by 1|desciption 1|Show|Edit|Destroy|
      |event 3|report_to 3|by 3|desciption 3|Show|Edit|Destroy|
      |event 4|report_to 4|by 4|desciption 4|Show|Edit|Destroy|


