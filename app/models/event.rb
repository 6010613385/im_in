class Event < ApplicationRecord
    has_many :comments,dependent: :destroy
    validates :title, presence: true
    validates :category, presence: true
    validates :description, presence: true
    validates :phone, presence: true 
  


end
