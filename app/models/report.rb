class Report < ApplicationRecord
     validates :event, presence: true
     validates :report_to, presence: true
     validates :by, presence: true
     validates :desciption, presence: true
end
