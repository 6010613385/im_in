json.extract! event, :id, :title, :category, :description, :by, :start, :end, :phone, :created_at, :updated_at
json.url event_url(event, format: :json)
