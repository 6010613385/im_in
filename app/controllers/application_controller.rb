class ApplicationController < ActionController::Base
  helper_method :current_user
  helper_method :authenticate_user!
  helper_method :authenticate_admin!
  
  def current_user
    begin
      if session[:register_id]
        @current_user ||= Register.find(session[:register_id])
      else
        @current_user = nil
      end
    rescue ActiveRecord::RecordNotFound => e
      puts e.message
      @current_user = nil

    end
  end
  
  def authenticate_user!
    begin
      current_user
      if current_user
      else
        redirect_to login_path,notice: 'You must be logged in frist.'
      end
    rescue ActiveRecord::RecordNotFound => e
      puts e.message
      @current_register = nil

    end
  end
    
  def authenticate_admin!
    begin
      current_user
      if current_user
      else
        redirect_to login_path
      end
    rescue ActiveRecord::RecordNotFound => e
      puts e.message
      @current_register = nil
    end
  end
  
end
