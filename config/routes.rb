Rails.application.routes.draw do
  get 'comment/body'
  resources :events do
    end
  #resources :comments, only: [:create, :index, :destroy]
  get 'login/index'
  resources :registers
  resources :reports
  resources :events
  resources :home
  
  #root 'users#index'
  #root 'events#index'
  #root 'reports#index'
  root 'events#index'
  #root 'registers#index'
  get 'static' => 'static_pages#static'
  get 'howto' => 'static_pages#howto'
  get  'about' => 'static_pages#about'
  get  'contact'  => 'static_pages#contact'
  get  'homepage'  => 'static_pages#homepage'
  #get 'users/index'
  #get 'events/index'
  #get 'reports/index'
  #get 'login/index'
  resources :sessions, only: [:new, :create, :destroy]
  get 'signup', to: 'registers#new', as: 'signup'
  get 'login', to: 'sessions#new', as: 'login'
  get 'logout', to: 'sessions#destroy', as: 'logout'
  get 'homeevent', to: 'static_pages#homepage', as: 'homeevent'
  get 'back', to: 'static_pages#static', as: 'back'
  get 'account', to: 'home#index', as: 'account'

end
