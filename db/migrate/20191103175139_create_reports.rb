class CreateReports < ActiveRecord::Migration[6.0]
  def change
    create_table :reports do |t|
      t.string :event
      t.string :report_to
      t.string :by
      t.text :desciption

      t.timestamps
    end
  end
end
