class CreateEvents < ActiveRecord::Migration[6.0]
  def change
    create_table :events do |t|
      t.string :title
      t.string :category
      t.text :description
      t.string :by
      t.datetime :start
      t.datetime :end
      t.string :phone

      t.timestamps
    end
  end
end
