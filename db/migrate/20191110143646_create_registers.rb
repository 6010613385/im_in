class CreateRegisters < ActiveRecord::Migration[6.0]
  def change
    create_table :registers do |t|
      t.string :email
      t.string :username
      t.string :password_digest
      t.string :phone
      t.string :image

      t.timestamps
      
    end
  end
end
