require 'rails_helper'

RSpec.describe CommentController, type: :controller do

  describe "GET #body" do
    it "returns http success" do
      get :body
      expect(response).to have_http_status(:ok)
    end
  end

end
