require 'rails_helper'

RSpec.describe "registers/edit", type: :view do
  before(:each) do
    @register = assign(:register, Register.create!(
      :email => "email@123.com",
      :username => "user",
      :password => "secret",
      :password_confirmation => "secret",
      :phone => "0985218956"
    ))
  end

  it "renders the edit register form" do
    render

    assert_select "form[action=?][method=?]", register_path(@register), "post" do

      assert_select "input[name=?]", "register[email]"

      assert_select "input[name=?]", "register[username]"

      assert_select "input[name=?]", "register[password]"
      
      assert_select "input[name=?]", "register[password_confirmation]"

      assert_select "input[name=?]", "register[phone]"
    end
  end
end
